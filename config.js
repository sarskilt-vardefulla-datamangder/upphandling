rdforms_specs.init({
    language: document.targetLanguage,
    namespaces: {
    },
    bundles: [
      ['../model2.json'],
    ],
    main: [
      'eNotice2-124',
      'eNotice2-125'
    ],
    supportive: [      
      'eNotice2-126'
    ]
  });
